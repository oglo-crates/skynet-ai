# SkyNet AI

A library for creating and training neural networks.



# Example:

```rust
use skynet_ai::*;

fn main() {
    let network = Network::new(&vec![
        (2, Activation::ReLU),
        (3, Activation::ReLU),
        (3, Activation::ReLU),
        (3, Activation::ReLU),
        (2, Activation::ReLU),
    ]);

    let input = vec![6.2, 1.7];
    let output = network.forward(&input);

    println!("{:?} -> {:?}", input, output);
}
```

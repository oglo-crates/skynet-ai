#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub enum Activation {
    Linear,
    ReLU,
}

impl Into<Box<dyn Fn(f64) -> f64>> for Activation {
    fn into(self) -> Box<dyn Fn(f64) -> f64> {
        return Box::new(match self {
            Self::Linear => { |o| o },

            Self::ReLU => {
                |o| {
                    if o > 0.0 {
                        o
                    } else {
                        0.0
                    }
                }
            },
        });
    }
}

use crate::{Activation, Layer};

#[derive(Debug, Clone, PartialEq)]
pub struct Network {
    pub layers: Vec<Layer>,
}

impl Network {
    pub fn new(layers: &[(usize, Activation)]) -> Self {
        return Self {
            layers: Self::layers_from_info(layers),
        };
    }

    pub fn forward(&self, inputs: &[f64]) -> Vec<f64> {
        let mut outputs: Vec<f64> = inputs.to_owned();

        for layer in self.layers.iter() {
            outputs = layer.forward(&outputs);
        }

        return outputs;
    }

    #[cfg(not(feature = "parallel"))]
    fn layers_from_info(layers_info: &[(usize, Activation)]) -> Vec<Layer> {
        let mut layers: Vec<Layer> = Vec::new();

        for i in 0..layers_info.len() {
            let current = layers_info[i];

            let prev = {
                if i != 0 {
                    layers_info[i - 1]
                }

                else {
                    current
                }
            };

            layers.push(Layer::new(current.0, prev.0, current.1));
        }

        return layers;
    }

    #[cfg(feature = "parallel")]
    fn layers_from_info(layers_info: &[(usize, Activation)]) -> Vec<Layer> {
        use rayon::prelude::*;

        let (tx, rx) = flume::unbounded();

        (0..layers_info.len()).into_par_iter().for_each_with(tx, |tx, i| {
            let current = layers_info[i];

            let prev = {
                if i != 0 {
                    layers_info[i - 1]
                }

                else {
                    current
                }
            };

            tx.send((i, Layer::new(current.0, prev.0, current.1))).unwrap();
        });

        let mut layers_pre: Vec<(usize, Layer)> = Vec::new();

        for _ in 0..layers_info.len() {
            layers_pre.push(rx.recv().unwrap());
        }

        layers_pre.sort_by(|a, b| a.partial_cmp(b).unwrap());

        let layers: Vec<Layer> = layers_pre.into_iter().map(|x| x.1).collect();

        return layers;
    }
}

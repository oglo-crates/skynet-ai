use rand::Rng;

use crate::Activation;

const WEIGHT_RANGE: f64 = 2.0;

#[derive(Debug, Clone, PartialEq, PartialOrd)]
pub struct Layer {
    pub activation: Activation,
    pub size: usize,
    pub weights: Vec<Vec<f64>>,
    pub biases: Vec<f64>,
}

impl Layer {
    #[cfg(not(feature = "parallel"))]
    pub fn new(size: usize, inputs: usize, activation: Activation) -> Self {
        return Self {
            size,
            activation,
            weights: {
                let mut weights: Vec<Vec<f64>> = Vec::new();

                let mut tr = rand::thread_rng();

                for _ in 0..size {
                    let mut weight: Vec<f64> = Vec::new();

                    for _ in 0..inputs {
                        weight.push(tr.gen_range(-WEIGHT_RANGE..=WEIGHT_RANGE));
                    }

                    weights.push(weight);
                }

                weights
            },
            biases: {
                let mut biases: Vec<f64> = Vec::new();

                for _ in 0..size {
                    biases.push(0.0);
                }

                biases
            },
        };
    }

    #[cfg(feature = "parallel")]
    pub fn new(size: usize, inputs: usize, activation: Activation) -> Self {
        use rayon::prelude::*;

        return Self {
            size,
            activation,
            weights: {
                let (tx, rx) = flume::unbounded();

                (0..size).into_par_iter().for_each_with(tx, |tx, _| {
                    let mut weight: Vec<f64> = Vec::new();

                    for _ in 0..inputs {
                        weight.push(rand::thread_rng().gen_range(-WEIGHT_RANGE..=WEIGHT_RANGE));
                    }

                    tx.send(weight).unwrap();
                });

                let mut weights: Vec<Vec<f64>> = Vec::new();

                for _ in 0..size {
                    weights.push(rx.recv().unwrap());
                }

                weights
            },
            biases: {
                let mut biases: Vec<f64> = Vec::new();

                for _ in 0..size {
                    biases.push(0.0);
                }

                biases
            },
        };
    }

    pub fn forward(&self, input: &[f64]) -> Vec<f64> {
        assert!(input.len() == self.weights[0].len());

        let mut outputs: Vec<f64> = Vec::new();

        for neuron in 0..self.size {
            let mut output: f64 = 0.0;

            for i in 0..input.len() {
                output += self.weights[neuron][i] * input[i];
            }

            output += self.biases[neuron];

            let activation: Box<dyn Fn(f64) -> f64> = self.activation.into();

            output = activation(output);

            outputs.push(output);
        }

        return outputs;
    }
}
